#include <calculatorgui.h>

#include <QApplication>
#include <iostream>

#define TEST

#ifdef TEST
#include "all_tests.h"
#endif

int main(int argc, char *argv[])
{
#ifdef TEST
    runAllTests();
#endif
    QApplication app(argc, argv);

    CalculatorGUI * calc = new CalculatorGUI;

    calc->resize(300, 250);
    calc->move(500, 500);
    calc->show();

    return app.exec();
}
