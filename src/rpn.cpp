#include "rpn.h"

#include <stack>

using std::stack;

int priority(const TokenType& type)
{
    switch (type)
    {
        case TokenType::ADD:
        case TokenType::SUBTRACT:
            return 1;
        case TokenType::DIVIDE:
        case TokenType::MULTIPLY:
            return 2;
        default:
            return 0;
    }
}

bool isPriorityHigher(const TokenType& lhs, const TokenType& rhs)
{
    return priority(lhs) > priority(rhs);
}

Tokens Rpn::fromInfixToPostfixNotation(const Tokens& tokens)
{
    Tokens result;
    stack<Token> operations;

    auto extractOperations = [&]() {
        while (!operations.empty())
        {
            if(operations.top().type == TokenType::OPEN_BRACKET)
            {
                break;
            }

            result.push_back(operations.top());
            operations.pop();
        }
    };

    auto extractEqualOperations = [&](const Token& token) {
        while(priority(operations.top().type) == priority(token.type))
        {
            result.push_back(operations.top());
            operations.pop();

            if(operations.empty())
            {
                break;
            }
        }
    };

    for(const auto& token : tokens)
    {
        if(isNumber(token.type))
        {
            result.push_back(token);
        }
        else if(operations.empty() ||
                token.type == TokenType::OPEN_BRACKET ||
                isPriorityHigher(token.type, operations.top().type))
        {
            operations.push(token);
        }
        else if(token.type == TokenType::CLOSE_BRACKET)
        {
            extractOperations();
            operations.pop();
        }
        else if(isPriorityHigher(operations.top().type, token.type))
        {
            extractOperations();
            operations.push(token);
        }
        else
        {
            extractEqualOperations(token);
            operations.push(token);
        }
    }

    while(!operations.empty())
    {
        result.push_back(operations.top());
        operations.pop();
    }

    return result;
}

/*
  1 * 2 / 3 - 4
  res     oper
|  -  | |     |
|  4  | |     |
|  *  | |     |
|  /  | |     |
|  3  | |     |
|  2  | |     |
|  1  | |     |
|_____| |_____|
*/
