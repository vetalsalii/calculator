#include "calculatorgui.h"
#include "calculator.h"
#include "tokenizer.h"
#include "rpn.h"

#include <iostream>

#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>

CalculatorGUI::CalculatorGUI(QWidget* parent)
    : QWidget (parent)
{
    line = new QLineEdit;
    line->setAlignment(Qt::AlignRight);
    line->setReadOnly(true);
    line->setMinimumSize(150, 50);

    const int sizeX = 4;
    const int sizeY = 5;

    QString buttons[sizeY][sizeX] = {
        {"(", ")", "CL", "C"},
        {"7", "8", "9", "/"},
        {"4", "5", "6", "*"},
        {"1", "2", "3", "-"},
        {"0", ".", "=", "+"}};

    //Layout setup
    QGridLayout* topLayout = new QGridLayout;
    topLayout->addWidget(line, 0, 0, 1, 4);

    for (int i = 0; i < sizeY; ++i)
    {
        for (int j = 0; j < sizeX; ++j)
        {
            topLayout->addWidget(createButton(buttons[i][j]), i + 1, j);
        }
    }
    setLayout(topLayout);

}

QPushButton* CalculatorGUI::createButton(const QString& str)
{
    QPushButton* pcmd = new QPushButton(str);
    pcmd->setMinimumSize(40, 40);
    connect(pcmd, SIGNAL(clicked()), SLOT(slotButtonClicked()));
    return pcmd;
}

void CalculatorGUI::slotButtonClicked()
{
    const QString str = static_cast<QPushButton*>(sender())->text();

    if(str == "=")
    {
        std::stringstream expr(this->expression.toStdString());
        const auto infix = Tokenizer::tokenizeToInfixNotation(expr);

        if(!Tokenizer::validParentheses(infix))
        {
            QMessageBox::warning(this, "Warning", "Wrong parentheses");
            return;
        }

        const auto postfix = Rpn::fromInfixToPostfixNotation(infix);
        try
        {
            double result = Calculator::calculate(postfix);
            expression.setNum(result);
        }
        catch (const std::exception& e)
        {
            Q_UNUSED(e);
            QMessageBox::warning(this, "Warning", "Wrong expression");
        }
    }
    else if(str == "CL")
    {
        expression.clear();
    }
    else if(str == "C")
    {
        if(!expression.isEmpty())
        {
            expression.chop(1);
        }
    }
    else
    {
        expression.push_back(str);
    }

    line->setText(expression);
    line->update();
}
