#include "token.h"

bool Token::operator ==(const Token &rhs)
{
    return type == rhs.type && value == rhs.value;
}

bool Token::operator !=(const Token &rhs)
{
    return !(*this == rhs);
}

bool isArithmeticOperation(const TokenType& type)
{
    switch (type)
    {
        case TokenType::ADD:
        case TokenType::DIVIDE:
        case TokenType::MULTIPLY:
        case TokenType::SUBTRACT:
            return true;
        default:
            return false;
    }
}

bool isNumber(const TokenType& type)
{
    return type == TokenType::NUMBER;
}
