#include "calculator.h"

#include <token.h>
#include <stdexcept>
#include <map>
#include <functional>
#include <cmath>

bool isEqual(double x, double y)
{
    return std::fabs(x - y) < 0.00000001;
}

double extractNumber(const Token& token)
{
    return std::stod(token.value);
}

const std::map<TokenType, std::function<double(double, double)>> operations {
{TokenType::ADD     , [](double x, double y) { return x + y; }},
{TokenType::SUBTRACT, [](double x, double y) { return x - y; }},
{TokenType::MULTIPLY, [](double x, double y) { return x * y; }},
{TokenType::DIVIDE  , [](double x, double y) { return x / y; }}
                                                                 };

void calculateTopStack(std::vector<double>& numbers, const TokenType& type)
{
    auto extractAndPopNumber = [](std::vector<double>& tokens)
    {
        double value = tokens.back();
        tokens.pop_back();
        return value;
    };

    if(numbers.empty() || numbers.size() < 2)
    {
        throw std::logic_error("Bad expression");
    }

    const double rhs = extractAndPopNumber(numbers);
    const double lhs = extractAndPopNumber(numbers);
    const auto operation = operations.at(type);

    if(type == TokenType::DIVIDE && isEqual(rhs, 0.0))
    {
        throw std::logic_error("Division be zero");
    }

    numbers.push_back(operation(lhs, rhs));
}

double Calculator::calculate(const Tokens &tokens)
{
    if(tokens.empty())
    {
        return 0;
    }

    std::vector<double> result;

    for(const auto& token : tokens)
    {
        if(isNumber(token.type))
        {
            result.push_back(extractNumber(token));
        }
        else if(isArithmeticOperation(token.type))
        {
            calculateTopStack(result, token.type);
        }
        else
        {
            throw std::logic_error("Unknown symbol " + token.value);
        }
    }

    if(result.empty() || result.size() != 1)
    {
        throw std::logic_error("Bad expression");
    }

    return result.back();
}
