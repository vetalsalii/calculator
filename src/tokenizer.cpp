#include "tokenizer.h"

#include <stack>

bool isUnaruMinus(const Tokens& tokens);
bool isOperator(int c);
bool isDot(int c);

TokenType charToTokenType(int c);

std::string extractNumber(std::istream &input)
{
    std::string result;

    while(isdigit(input.peek()) || isDot(input.peek()))
    {
        char c;
        input >> c;

        if(result.empty() && c == '.')
        {
            result += "0";
        }

        result += c;
    }

    return result;
}

Tokens Tokenizer::tokenizeToInfixNotation(std::istream &input)
{
    Tokens tokens;

    char c;
    while(input >> c)
    {
        if(isdigit(c) || isDot(c))
        {
            input.unget();
            const auto number = extractNumber(input);
            tokens.push_back({ number, TokenType::NUMBER });
        }
        else if(isOperator(c))
        {
            if(c == '-' && isUnaruMinus(tokens))
            {
                const auto number = "-" + extractNumber(input);
                tokens.push_back({ number, TokenType::NUMBER });
            }
            else
            {
                tokens.push_back({ std::string{} + c,  charToTokenType(c) });
            }
        }
        else
        {
            tokens.push_back({ std::string{} + c,  charToTokenType(c) });
        }
    }

    return tokens;
}



bool Tokenizer::validParentheses(const Tokens &expression)
{
    auto topAndPop = [](std::stack<TokenType>& parentheses)
    {
        auto type = parentheses.top();
        parentheses.pop();
        return type;
    };

    std::stack<TokenType> parentheses;

    for(const auto& token : expression)
    {
        auto type = token.type;
        if(type == TokenType::OPEN_BRACKET || type == TokenType::CLOSE_BRACKET)
        {
            if(type == TokenType::OPEN_BRACKET)
            {
                parentheses.push(TokenType::CLOSE_BRACKET);
            }
            else if(parentheses.empty() || topAndPop(parentheses) != type)
            {
                return false;
            }
        }
    }

    return parentheses.empty();
}

bool isUnaruMinus(const Tokens& tokens)
{
    if(tokens.empty() ||
       tokens.back().type == TokenType::OPEN_BRACKET ||
       isArithmeticOperation(tokens.back().type))
    {
        return true;
    }

    return false;
}

bool isOperator(int c)
{
    switch (c)
    {
        case '+': case '-':
        case '*': case '/':
            return true;
    }

    return false;
}

/* TODO
    // Percent
    // pow
    // sin
    // cos
    // sqrt
*/

TokenType charToTokenType(int c)
{
    switch (c)
    {
        case '+': return TokenType::ADD;
        case '-': return TokenType::SUBTRACT;
        case '*': return TokenType::MULTIPLY;
        case '/': return TokenType::DIVIDE;
        case '(': return TokenType::OPEN_BRACKET;
        case ')': return TokenType::CLOSE_BRACKET;
        default : return TokenType::UNKNOWN;
    }
}

bool isDot(int c)
{
    return c == '.';
}

