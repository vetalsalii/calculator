TEMPLATE = app

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_LFLAGS_CONSOLE += -fsanitize=address

CONFIG += c++17
CONFIG -= app_bundle

INCLUDEPATH += . include src test

HEADERS += \
    include/tokenizer.h \
    include/token.h \
    include/all_tests.h \
    include/rpn.h \
    include/calculator.h \
    include/calculatorgui.h

SOURCES += \
        src/main.cpp \
        src/tokenizer.cpp \
        src/token.cpp \
        src/rpn.cpp \
        src/calculator.cpp \
        src/calculatorgui.cpp \
        test/rpn_test.cpp \
        test/token_test.cpp \
        test/tokenizer_test.cpp \
        test/calculator_test.cpp \
