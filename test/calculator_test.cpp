#include <tokenizer.h>
#include <rpn.h>

#include <calculator.cpp>
#include <cassert>
#include <iostream>
#include <stdexcept>

using std::vector;

void operationsTest()
{
    const auto& operationAdd      = operations.at(TokenType::ADD);
    const auto& operationDivide   = operations.at(TokenType::DIVIDE);
    const auto& operationSubtract = operations.at(TokenType::SUBTRACT);
    const auto& operationMultiply = operations.at(TokenType::MULTIPLY);

    double x = 10;
    double y = 4;

    assert(isEqual(operationAdd     (x, y), 14.0));
    assert(isEqual(operationDivide  (x, y), 2.5));
    assert(isEqual(operationSubtract(x, y), 6.0));
    assert(isEqual(operationMultiply(x, y), 40.0));
}

void calculateTopStackTest()
{
    const auto add      = TokenType::ADD;
    const auto divide   = TokenType::DIVIDE;
    const auto multiply = TokenType::MULTIPLY;
    const auto subtract = TokenType::SUBTRACT;

    {
        vector<double> tokens {10, 4};
        calculateTopStack(tokens, add);
        assert(isEqual(tokens.back(), 14.0));
    }

    {
        vector<double> tokens {10, 4};
        calculateTopStack(tokens, multiply);
        assert(isEqual(tokens.back(), 40.0));
    }

    {
        vector<double> tokens {10, 4};
        calculateTopStack(tokens, subtract);
        assert(isEqual(tokens.back(), 6.0));
    }

    {
        vector<double> tokens {1, 0};

        try
        {
            calculateTopStack(tokens, divide);
        }
        catch (const std::logic_error& e)
        {
            (void)e;
        }
        catch(...)
        {
            std::cerr << "Calculator Fail"
                      << __FILE__ << '\n'
                      << __LINE__ << std::endl;
            exit(-1);
        }
    }
}

void calculateDivisionByZeroTest()
{
    {
        std::stringstream ss("10/0");
        auto tokens = Tokenizer::tokenizeToInfixNotation(ss);
        tokens = Rpn::fromInfixToPostfixNotation(tokens);

        try
        {
            Calculator::calculate(tokens);
            assert(false);
        }
        catch (const std::exception& e)
        {
            (void)e;
        }
    }

    {
        std::stringstream ss("1+2-3/(4-4)");
        auto tokens = Tokenizer::tokenizeToInfixNotation(ss);
        tokens = Rpn::fromInfixToPostfixNotation(tokens);

        try
        {
            Calculator::calculate(tokens);
            assert(false);
        }
        catch (const std::exception& e)
        {
            (void)e;
        }
    }
}

void calculateBadExpressionTest()
{
    {
        std::stringstream ss("10 / 0-");
        const auto infix = Tokenizer::tokenizeToInfixNotation(ss);
        const auto postfix = Rpn::fromInfixToPostfixNotation(infix);

        try
        {
            Calculator::calculate(postfix);
            assert(false);
        }
        catch (const std::exception& e)
        {
            (void)e;
        }
    }

    {
        std::stringstream ss("+10/2");
        const auto infix = Tokenizer::tokenizeToInfixNotation(ss);
        const auto postfix = Rpn::fromInfixToPostfixNotation(infix);

        try
        {
            Calculator::calculate(postfix);
            assert(false);
        }
        catch (const std::exception& e)
        {
            (void)e;
        }
    }
}

void calculateTest()
{
    {
        std::stringstream ss("10+15");
        const auto infix = Tokenizer::tokenizeToInfixNotation(ss);
        const auto postfix = Rpn::fromInfixToPostfixNotation(infix);

        auto result = Calculator::calculate(postfix);
        double expected = 25;
        assert(isEqual(expected, result));
    }

    {
        std::stringstream ss("1-2");
        const auto infix = Tokenizer::tokenizeToInfixNotation(ss);
        const auto postfix = Rpn::fromInfixToPostfixNotation(infix);

        auto result = Calculator::calculate(postfix);
        double expected = -1;
        assert(isEqual(expected, result));
    }
}

void calculatorTest()
{
    operationsTest();
    calculateTopStackTest();
    calculateDivisionByZeroTest();
    calculateBadExpressionTest();
    calculateTest();
    std::cerr << "Calculator OK" << std::endl;
}
