#include "token.cpp"
#include <cassert>
#include <iostream>

// TokenType

void tokenTest()
{
    assert((Token{"1", TokenType::NUMBER}) == (Token{"1", TokenType::NUMBER}));
    assert((Token{"1", TokenType::NUMBER}) != (Token{"2", TokenType::NUMBER}));
    assert((Token{"-", TokenType::SUBTRACT}) != (Token{"+", TokenType::ADD}));
    assert((Token{"*", TokenType::MULTIPLY}) != (Token{"/", TokenType::DIVIDE}));
    assert((Token{"(", TokenType::OPEN_BRACKET}) != (Token{")", TokenType::CLOSE_BRACKET}));

    std::cerr << "Token OK" << std::endl;
}
