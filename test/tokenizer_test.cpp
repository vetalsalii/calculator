#include "tokenizer.cpp"
#include <cassert>
#include <iostream>

using namespace std;

void isOperatorTest()
{
    assert(isOperator('-') == true);
    assert(isOperator('+') == true);
    assert(isOperator('*') == true);
    assert(isOperator('/') == true);

    assert(isOperator('(') == false);
    assert(isOperator(')') == false);
    assert(isOperator('=') == false);
    assert(isOperator('1') == false);
}

void charToTOkenTest()
{
    assert(charToTokenType('+') == TokenType::ADD);
    assert(charToTokenType('/') == TokenType::DIVIDE);
    assert(charToTokenType('*') == TokenType::MULTIPLY);
    assert(charToTokenType('-') == TokenType::SUBTRACT);
    assert(charToTokenType('(') == TokenType::OPEN_BRACKET);
    assert(charToTokenType(')') == TokenType::CLOSE_BRACKET);
    assert(charToTokenType('=') == TokenType::UNKNOWN);
    assert(charToTokenType('1') == TokenType::UNKNOWN);
}

void isUnaruMinusTest()
{
    assert(isUnaruMinus({}) == true);

    assert(isUnaruMinus({ {"(", TokenType::OPEN_BRACKET} }) == true);

    assert(isUnaruMinus({ {"1", TokenType::NUMBER },
                          {"+", TokenType::ADD}}
                        ) == true);

    assert(isUnaruMinus({ {"6", TokenType::NUMBER } }) == false);
    assert(isUnaruMinus({ {")", TokenType::NUMBER } }) == false);
}

void extractNumberTest()
{
    {
        string expression = "125+13";
        stringstream stream(expression);
        string number = extractNumber(stream);
        assert(number == "125");
    }

    {
        string expression = "-123";
        stringstream stream(expression);
        string number = extractNumber(stream);
        assert(number.empty());
    }

    {
        string expression = "12.5";
        stringstream stream(expression);
        string number = extractNumber(stream);
        assert(number == "12.5");
    }

    {
        string expression = ".5";
        stringstream stream(expression);
        string number = extractNumber(stream);
        assert(number == "0.5");
    }
}

void isDotTest()
{
    assert(isDot('.') == true);
    assert(isDot('2') == false);
    assert(isDot('_') == false);
}

void tokenizeTest()
{
    {
        stringstream ss("1 + 2 / 2");
        vector<Token> tokens = Tokenizer::tokenizeToInfixNotation(ss);

        assert((tokens[0] == Token{string("1"), TokenType::NUMBER}));
        assert((tokens[1] == Token{string("+"), TokenType::ADD}));
        assert((tokens[2] == Token{string("2"), TokenType::NUMBER}));
        assert((tokens[3] == Token{string("/"), TokenType::DIVIDE}));
        assert((tokens[4] == Token{string("2"), TokenType::NUMBER}));
    }

    {
        stringstream ss("-1 + -2 - 3 + -4");
        vector<Token> tokens = Tokenizer::tokenizeToInfixNotation(ss);

        assert((tokens[0] == Token{string("-1"), TokenType::NUMBER}));
        assert((tokens[1] == Token{string("+") , TokenType::ADD}));
        assert((tokens[2] == Token{string("-2"), TokenType::NUMBER}));
        assert((tokens[3] == Token{string("-") , TokenType::SUBTRACT}));
        assert((tokens[4] == Token{string("3") , TokenType::NUMBER}));
        assert((tokens[5] == Token{string("+") , TokenType::ADD}));
        assert((tokens[6] == Token{string("-4"), TokenType::NUMBER}));
    }

    {
        stringstream ss("1 + 2 * 3 / 4 - 5");
        vector<Token> tokens = Tokenizer::tokenizeToInfixNotation(ss);

        assert((tokens[0] == Token{string("1"), TokenType::NUMBER}));
        assert((tokens[1] == Token{string("+"), TokenType::ADD}));
        assert((tokens[2] == Token{string("2"), TokenType::NUMBER}));
        assert((tokens[3] == Token{string("*"), TokenType::MULTIPLY}));
        assert((tokens[4] == Token{string("3"), TokenType::NUMBER}));
        assert((tokens[5] == Token{string("/"), TokenType::DIVIDE}));
        assert((tokens[6] == Token{string("4"), TokenType::NUMBER}));
        assert((tokens[7] == Token{string("-"), TokenType::SUBTRACT}));
        assert((tokens[8] == Token{string("5"), TokenType::NUMBER}));
    }

    {
        stringstream ss("(10 + 20) / 2)");
        vector<Token> tokens = Tokenizer::tokenizeToInfixNotation(ss);

        assert((tokens[0] == Token{string("(") , TokenType::OPEN_BRACKET}));
        assert((tokens[1] == Token{string("10"), TokenType::NUMBER}));
        assert((tokens[2] == Token{string("+") , TokenType::ADD}));
        assert((tokens[3] == Token{string("20"), TokenType::NUMBER}));
        assert((tokens[4] == Token{string(")") , TokenType::CLOSE_BRACKET}));
        assert((tokens[5] == Token{string("/") , TokenType::DIVIDE}));
        assert((tokens[6] == Token{string("2") , TokenType::NUMBER}));
    }

    {
        stringstream ss(".11 + 5.)");
        vector<Token> tokens = Tokenizer::tokenizeToInfixNotation(ss);

        assert((tokens[0] == Token{string("0.11") , TokenType::NUMBER}));
        assert((tokens[1] == Token{string("+"), TokenType::ADD}));
        assert((tokens[2] == Token{string("5.") , TokenType::NUMBER}));
    }

    {
        stringstream ss("1.55 + 1.45 * (1.5 + 0.5)");
        vector<Token> tokens = Tokenizer::tokenizeToInfixNotation(ss);

        assert((tokens[0] == Token{string("1.55") , TokenType::NUMBER}));
        assert((tokens[1] == Token{string("+"), TokenType::ADD}));
        assert((tokens[2] == Token{string("1.45") , TokenType::NUMBER}));
        assert((tokens[3] == Token{string("*") , TokenType::MULTIPLY}));
        assert((tokens[4] == Token{string("(") , TokenType::OPEN_BRACKET}));
        assert((tokens[5] == Token{string("1.5") , TokenType::NUMBER}));
        assert((tokens[6] == Token{string("+") , TokenType::ADD}));
        assert((tokens[7] == Token{string("0.5") , TokenType::NUMBER}));
        assert((tokens[8] == Token{string(")") , TokenType::CLOSE_BRACKET}));
    }

    {
        stringstream ss("30 - 10%");
        vector<Token> tokens = Tokenizer::tokenizeToInfixNotation(ss);

        assert((tokens[0] == Token{string("30") , TokenType::NUMBER}));
        assert((tokens[1] == Token{string("-") , TokenType::SUBTRACT}));
        assert((tokens[2] == Token{string("10") , TokenType::NUMBER}));
        assert((tokens[3] == Token{string("%") , TokenType::UNKNOWN}));
    }
}

void tokenizerTest()
{
    isOperatorTest();
    charToTOkenTest();
    isUnaruMinusTest();
    extractNumberTest();
    tokenizeTest();
    cerr << "Tokenizer OK" << endl;
}
