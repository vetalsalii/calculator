#include "rpn.cpp"

#include <cassert>

#include <iostream>

using namespace std;

void isNUmberTest()
{
    assert(isNumber(TokenType::NUMBER) == true);
    assert(isNumber(TokenType::UNKNOWN) == false);
    assert(isNumber(TokenType::DIVIDE) == false);
}

void priorityTest()
{
    assert(priority(TokenType::ADD) == 1);
    assert(priority(TokenType::SUBTRACT) == 1);
    assert(priority(TokenType::DIVIDE) == 2);
    assert(priority(TokenType::MULTIPLY) == 2);
}

void isPriorityHigherTest()
{
    assert(isPriorityHigher(TokenType::MULTIPLY, TokenType::ADD) == true);
    assert(isPriorityHigher(TokenType::ADD, TokenType::MULTIPLY) == false);
    assert(isPriorityHigher(TokenType::ADD, TokenType::ADD) == false);
    assert(isPriorityHigher(TokenType::MULTIPLY, TokenType::MULTIPLY) == false);
}

void fromInfixToPostfixTest()
{
    {
        Tokens infix {
            Token{"3", TokenType::NUMBER},
            Token{"+", TokenType::ADD},
            Token{"2", TokenType::NUMBER}
        };

        const auto postfix = Rpn::fromInfixToPostfixNotation(infix);

        assert(postfix[0].value == "3");
        assert(postfix[1].value == "2");
        assert(postfix[2].value == "+");
    }

    {
        // ((15 ÷ (7 − (1 + 1))) × 3) − (2 + (1 + 1))
        Tokens infix {
            Token{"(", TokenType::OPEN_BRACKET},
            Token{"(", TokenType::OPEN_BRACKET},
            Token{"15", TokenType::NUMBER},
            Token{"/", TokenType::DIVIDE},
            Token{"(", TokenType::OPEN_BRACKET},
            Token{"7", TokenType::NUMBER},
            Token{"-", TokenType::SUBTRACT},
            Token{"(", TokenType::OPEN_BRACKET},
            Token{"1", TokenType::NUMBER},
            Token{"+", TokenType::ADD},
            Token{"1", TokenType::NUMBER},
            Token{")", TokenType::CLOSE_BRACKET},
            Token{")", TokenType::CLOSE_BRACKET},
            Token{")", TokenType::CLOSE_BRACKET},
            Token{"*", TokenType::MULTIPLY},
            Token{"3", TokenType::NUMBER},
            Token{")", TokenType::CLOSE_BRACKET},
            Token{"-", TokenType::SUBTRACT},
            Token{"(", TokenType::OPEN_BRACKET},
            Token{"2", TokenType::NUMBER},
            Token{"+", TokenType::ADD},
            Token{"(", TokenType::OPEN_BRACKET},
            Token{"1", TokenType::NUMBER},
            Token{"+", TokenType::ADD},
            Token{"1", TokenType::NUMBER},
            Token{")", TokenType::CLOSE_BRACKET},
            Token{")", TokenType::CLOSE_BRACKET},
        };

        const auto postfix = Rpn::fromInfixToPostfixNotation(infix);

        assert(postfix[0].value  == "15");
        assert(postfix[1].value  == "7");
        assert(postfix[2].value  == "1");
        assert(postfix[3].value  == "1");
        assert(postfix[4].value  == "+");
        assert(postfix[5].value  == "-");
        assert(postfix[6].value  == "/");
        assert(postfix[7].value  == "3");
        assert(postfix[8].value  == "*");
        assert(postfix[9].value  == "2");
        assert(postfix[10].value == "1");
        assert(postfix[11].value == "1");
        assert(postfix[12].value == "+");
        assert(postfix[13].value == "+");
        assert(postfix[14].value == "-");
    }
}

void prnTests()
{
    fromInfixToPostfixTest();
    isNUmberTest();
    priorityTest();
    isPriorityHigherTest();
    cerr << "RPN OK" << endl;
}
