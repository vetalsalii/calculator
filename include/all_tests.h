#ifndef ALL_TESTS_H
#define ALL_TESTS_H

void runAllTests()
{
    void tokenizerTest();
    void tokenTest();
    void prnTests();
    void calculatorTest();

    tokenizerTest();
    tokenTest();
    prnTests();
    calculatorTest();
}

#endif // ALL_TESTS_H
