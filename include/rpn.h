#ifndef RPN_H
#define RPN_H

#include "token.h"

#include <vector>

using Tokens = std::vector<Token>;

//! @brief  Reverse Polish notation, RPN
class Rpn
{
public:
    static Tokens fromInfixToPostfixNotation(const Tokens& tokens);
};

#endif // RPN_H
