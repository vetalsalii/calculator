#ifndef CALCULATORGUI_H
#define CALCULATORGUI_H

#include <QWidget>
#include <QStack>

class QLineEdit;
class QPushButton;

class CalculatorGUI : public QWidget
{
    Q_OBJECT
public:
    CalculatorGUI(QWidget* parent = nullptr);

private:
    QLineEdit* line;
    QString expression;
    QString display;

    QPushButton* createButton(const QString& str);
public slots:
    void slotButtonClicked();
};

#endif // CALCULATORGUI_H
