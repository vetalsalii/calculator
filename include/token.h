#ifndef TOKEN_H
#define TOKEN_H

#include <string>

enum class TokenType
{
    UNKNOWN,
    NUMBER,
    DIVIDE,
    ADD,
    SUBTRACT,
    MULTIPLY,
    OPEN_BRACKET,
    CLOSE_BRACKET,
    END_TOKEN_TYPE
};

struct Token
{
    std::string value;
    TokenType type;
    bool operator == (const Token& rhs);
    bool operator != (const Token& rhs);
};

bool isArithmeticOperation(const TokenType& type);
bool isNumber(const TokenType& type);

#endif // TOKEN_H
