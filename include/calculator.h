#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <vector>

using Tokens = std::vector<class Token>;

class Calculator
{
public:
    static double calculate(const Tokens& tokens);
};

#endif // CALCULATOR_H
