#ifndef TOKENIZER_H
#define TOKENIZER_H

#include "token.h"

#include <vector>
#include <sstream>

using Tokens = std::vector<Token>;

class Tokenizer
{
public:
    static Tokens tokenizeToInfixNotation(std::istream& input);
    static bool validParentheses(const Tokens& expression);
};

#endif // TOKENIZER_H
